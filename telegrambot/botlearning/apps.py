from django.apps import AppConfig


class BotlearningAppConfig(AppConfig):
    name = "telegrambot.botlearning"
