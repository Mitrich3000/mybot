import json
import random
import telebot

import requests
from django.http.response import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

WEBHOOK_HOST = '995876b6.ngrok.io'
TELEGRAM_TOKEN = '779237281:AAFIY55Bke0O1dTW0HO9h5y7qKdrqgkw6W8'
bot = telebot.TeleBot(TELEGRAM_TOKEN)
bot.remove_webhook()
bot.set_webhook(url=WEBHOOK_HOST)
print('start_hook')
print(bot.get_webhook_info())


# Приветственная надпись
@bot.message_handler(commands=['start'])
def start(message):
    sent = bot.send_message(message.chat.id, 'Как тебя зовут?')
    bot.register_next_step_handler(sent, hello)


def hello(message):
    bot.send_message(message.chat.id, 'Привет, {name}. Рад тебя видеть.'.format(name=message.text))


class TelegramBotView(generic.View):
    # csrf_exempt is necessary because the request comes from the Telegram server.
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle messages in whatever format they come
    def post(self, request, bot_token):
        raw = request.body.decode('utf-8')
        try:
            update = telebot.types.Update.de_json(raw)
        except ValueError:
            HttpResponseBadRequest('Invalid Request Body!')
        else:
            bot.process_new_updates([update])

        return JsonResponse({}, status=200)

    def get(self, request, *args, **kwargs):
        return HttpResponse("Hello World!")
